#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


setup(name='firstTest',
      version='1.0',
      description='Lib',
      long_description='Lib',
      license='closed',
      author='marcyyin',
      author_email='marcy@dd.com',
      namespace_packages=[],
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'Twisted==17.1.0',
          'requests==2.10.0',
          'lxml==3.2.2',
          'service_identity',
          ]
      )
